To run this solution succesfully you MUST do so in python 3

EX:
python3 solvePuzzle.py

Once the solution is running you must provide a valid text file or it will keep asking you until you do, if you have no provided one you will be forced to exit the program manually as it as designed to accept a valid text file given the parameters. Once it has solved the wordsearch the program will quit appropriately.