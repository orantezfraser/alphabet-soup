'''
Orantez Fraser solution to wordsearch

How I attempted this solution was by breaking defining goals and breaking the problem into parts


Part 1 : Validating files/reading input
Part 2: Parsing File, creating wordbank and 2 dimensional array
Part 3: Solving Puzzle
Part 4: Printing Solution
Part 5: Testing


While doing part 3 I realized my inital implementation which was basically cascading for loops wasn't very efficient so
I researched online for something potentially better and pretty much the best solutions were using named tuples and 
either enumerations or cartesian products, both of which were more efficient and readable than my original solution

Source -> https://codereview.stackexchange.com/questions/156176/word-search-solver

I used the solve methods found in this stackoverflow post and modified it to solve for not just the starting letter coordinate but the 
second letter coordinate as well

I tested with a number of files, regular square grids,rectangular grids, large grids, and empty grids.

'''


from collections import namedtuple
from itertools import product
import re
import sys
import os.path
from os import path

Direction = namedtuple('Direction', 'di dj name')

DIRECTIONS = [
    Direction(-1, -1, "up and to the left"),
    Direction(-1,  0, "up"),
    Direction(-1, +1, "up and to the right"),
    Direction( 0, -1, "left"),
    Direction( 0, +1, "right"),
    Direction(+1, -1, "down and to the left"),
    Direction(+1,  0, "down"),
    Direction(+1, +1, "down and to the right"),
]

#Returns every combination of letters that matches the word length in the grid
def extract(grid, i, j, dir, length):
    """
    Extract letters from the grid, starting at row i column j, as a string.
    If the extraction will walk out of bounds, return None.
    """
    if ( 0 <= i + (length - 1) * dir.di < len(grid) and
         0 <= j + (length - 1) * dir.dj < len(grid[i]) ):
        return ''.join(
            grid[i + n * dir.di][j + n * dir.dj] for n in range(length)
        )
    return None

#Returns a match along with the row and col if found
def search(grid, word):
    """
    Search for a word in a grid, returning a tuple of the starting row,
    starting column, and direction.  If the word is not found, return None.
    """
    word_len = len(word)
    for i, j, dir in product(range(len(grid)), range(len(grid[0])), DIRECTIONS):
        if word == extract(grid, i, j, dir, word_len):
            return i, j, dir
    return None

#Parses text file and returns wordbank and 2 dimensional array of letters
def readGrid(fileName):
    file1 = open(fileName, 'r')
    lineCount = 0
    rowCount = 0
    letterGrid = []
    wordBank = []
    for line in file1:
        if(lineCount == 0):
            holder = line.strip().split("x")
            rowCount = int(holder[0])
        elif(lineCount <= rowCount):
            letterGrid.append(line.strip().split(" "))
        else:
            wordBank.append(line.strip())
        lineCount +=1
    return wordBank, letterGrid

def solvePuzzle(filename):
    wordBank, grid = readGrid(filename)
    for word in wordBank:
        match = search(grid, word.upper())
        #Only print out valid matches, ignore otherwise
        if match is not None:
            i, j, dir = match
            seper = len(word) - 1 #Distance between first and last letter

            #Calculate the row and col of the last letter based on direction and distance
            i2 = i + (dir.di * seper)
            j2 = j + (dir.dj * seper)

            #Print out the word and coordinates
            print(word + " " + str(i)+":"+str(j) + " " + str(i2)+":"+str(j2))

     
#Validate file exists and solve puzzle
while(True):
    fileName = str(input("Please provide the name of a valid file that contains a puzzle to be solved: "))
    if(path.exists(fileName)):
        break
    else:
        print("Invalid file name, please try again!")
solvePuzzle(fileName)